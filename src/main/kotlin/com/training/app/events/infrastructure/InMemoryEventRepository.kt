package com.training.app.events.infrastructure

import com.training.app.events.domain.Event
import com.training.app.events.domain.EventsRepository
import com.training.app.infrastructure.BaseInMemoryRepository
import java.util.UUID

class InMemoryEventRepository : EventsRepository, BaseInMemoryRepository<Event, UUID>()