package com.training.app.events.domain

import java.util.UUID

interface EventsRepository {
    fun save(entity: Event): Event
    fun findByIdOrNull(id: UUID): Event?
    fun findAll(): List<Event>
    fun deleteById(id: UUID)
}