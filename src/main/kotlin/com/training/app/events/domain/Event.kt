package com.training.app.events.domain

import java.time.Instant
import java.time.ZonedDateTime
import java.util.UUID

data class EventDescription(
    val name: String,
    val date: ZonedDateTime,
    val categoryName: String
)

data class ReservationConfirmation(
    val id: UUID,
    val waitingList: Boolean,
    val notificationEmail: String
)

data class EventSummary(
    val id: UUID,
    val description: EventDescription,
    val availablePlaces: Int
)

class Reservation(
    val id: UUID,
    val notificationEmail: String,
    var waitingList: Boolean,
    val reservationDate: Instant
)

class Event(
    val id: UUID,
    var description: EventDescription,
    private val initialAvailablePlaces: Int
) {
    private val reservations = mutableListOf<Reservation>()

    private val availablePlaces: Int
        get() = initialAvailablePlaces - reservations.count { !it.waitingList }

    val summary: EventSummary
        get() = EventSummary(id, description, availablePlaces)

    fun reservePlace(notificationEmail: String): ReservationConfirmation {
        val reservation = Reservation(UUID.randomUUID(), notificationEmail, availablePlaces <= 0, Instant.now())
        reservations.add(reservation)
        return ReservationConfirmation(reservation.id, reservation.waitingList, reservation.notificationEmail)
    }

    fun cancelReservation(reservationId: UUID) {
        reservations.removeIf { it.id == reservationId }
        if (availablePlaces > 0) {
            val firstWaitingReservation = reservations
                .filter { it.waitingList }
                .minByOrNull { it.reservationDate }

            firstWaitingReservation?.let { it.waitingList = false }
        }
    }
}
