package com.training.app.infrastructure

import java.util.Collections
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties

open class BaseInMemoryRepository<Entity, Id> {
    private val data = Collections.synchronizedMap(mutableMapOf<Id, Entity>())

    fun save(entity: Entity): Entity {
        data[readInstanceProperty<Id>(entity!!, "id")] = entity
        return entity
    }

    fun findByIdOrNull(id: Id): Entity? = data[id]

    fun findAll(): List<Entity> = data.values.toList()

    fun deleteById(id: Id) {
        data.remove(id)
    }
}

@Suppress("UNCHECKED_CAST")
private fun <R> readInstanceProperty(instance: Any, propertyName: String): R {
    val property = instance::class.memberProperties
        .first { it.name == propertyName } as KProperty1<Any, *>
    return property.get(instance) as R
}