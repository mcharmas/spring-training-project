# Spring - zadania

## Projekt aplikacji Web

### Wizja

Aplikacja służąca do rezerwacji miejsc na wydarzenia w organizacji
(np. wydarzenia sportowe, angielski etc). Użytkownicy bez logowania/rejestracji
mogą przeglądać listę wydarzeń, dodawać wydarzenia. Anulować i edytować wydarzenie może tylko
użytkownik, który je stworzył.

W momencie, gdy wszyskie miejsca na wydarzenie będą już zajęte użytkownik, który dokonuje
rezerwacji trafia na listę oczekujących. Gdy inny użytkownik wycofa swoją rezerwację
użytkownicy w listy oczekujących trafiają na wydarzenie i są o tym powiadomieni.

Dodatkowo wydarzenia są kategoryzowane. Użytkownicy mogą wylistować kategorie wydarzeń
i zapisać się na powiadomienia o nowych wydarzeniach w danej kategorii.

### Moduły

* wydarzenia i rejestracje
* kategorie
* powiadomienia
* zarządzanie dostępem do wydarzeń

## Zadanie 1

Zaimplementuj bean `EventService`, który będzie zawierał zachowanie umożliwiające:

* tworzenie wydarzenia
* listowanie wydarzeń (`EventSummary`)
* anulowanie wydarzenia (bez kontroli dostępu)
* aktualizację wydarzenia (bez kontroli dostępu)

Do implementacji użyj zadeklarowanego repozytorium i `InMemoryEventRepository` jako jego implementacji.

Skonfiguruj i wstrzyknij zależności za pomocą dependency injection.

Przetestuj serwis wraz z zależnościami testami funkcjonalnymi `@SpringBootTest`. 

## Zadanie 2

Po dodaniu wydarzenia z kategorią w module kategorii powinna zostać ona utworzona.

Zaimplementuj `CategoryNotificationService`, `Category` oraz `CategoryRepository`.
Zintegruj moduły za pomocą zdarzeń.

Tak utworzone kategorie powinno dać się listować, subskrybować na powiadomienia 
i usuwać subskrypcje.

Dodanie wydarzenia dla kategorii, która ma subskrybentów powinna skutkować
kolejnym zdarzeniem (`NewEventFromCategorySubmitted`) zawierającą identyfikator
wydarzenia, nazwę kategorii oraz zbiór subskrybentów.

Dopisz testy funkcjonalne weryfikujące zachowanie.

## Zadanie 3

Zaimplementuj moduł wysyłający maile na dwa sposoby (`interface MailSender`):

* developerski - treść maila wyświetlana jest na konsolę
* produkcyjny - udający produkcyjny, też drukujący treść maila na konsolę

Wiadomość do wysłania powinna mieć następujący format:

```kotlin
data class EmailToSend(
    val recipient: String,
    val templateId: String,
    val data: Map<String, Any>
)
```

Implementacja produkcyjna powinna być dostępna tylko jeśli profil `email-production` jest włączony. W przeciwnym
razie powinna zostać używa implementacja developerska.

Utwórz konfigurację aplikacji dla profilu `production`, która włącza profil `email-production`. 

Użyj utworzony moduł do wysyłania powiadomienia o wydarzeniach w danej kategorii (`NewEventFromCategorySubmitted`)
tworząc w warstwie infrastruktury modułu `categories` odpowiedni EventListener, który będzie używał
zaimplementowany `EmailSender` do wysyłania wiadomości. Dane wiadomości wypełnij nazwą kategorii oraz identyfikatorem
wydarzenia.

### Zadanie 4

Tworzone wydarzenia zawiera liczbę miejsc dostępnych na to wydarzenie. Jeśli liczba miejsc
nie zostanie dostarczona podczas tworzenia wydarzenia powinna ona zostać ustawiona wartość domyślna.

Wartość domyślna powinna być konfigurowalna z właściwości aplikacji.

Zdefiniuj bean `EventProperties` zawierający konfigurację i ustaw ją na 10 we właściwościach
aplikacji.

Przetestuj zachowanie dopisując funkcjonalne testy automatyczne.

### Zadanie 5

Utwórz moduł `access`, który będzie zajmował się kontrolą dostępu do wydarzeń.

Zdefiniuj w nim:

```kotlin
interface EventAccessPolicy {
    fun bindEvent(eventId: UUID)

    fun assertHasWriteAccessTo(eventId: UUID)

    class EventAccessForbiddenException: RuntimeException()
}
```

która będzie odpowaidała za decydowanie o dostępie do danego eventu.

Zaimplementuj aspekt, używający `EventAccessPolicy` i owijający dwie metody `EventService`:

* tworzącą event (powinna bindować zdarzenie do polityki)
* modyfikująca event (powinna robić asercję)

Zaimplementuj politykę tak, aby wiązała wydarzenia z sesją użytkownika.

Przetestuj funkcjonalnie zaimplementowane zachowanie wstrzykując własną, testową implementację
`EventAccessPolicy`. 

### Zadanie 6

Zmodyfikuj logikę wydarzenia tak, aby w momencie, gdy ktoś z listy oczekujących
awansuje i dostanie rezerwacje wyemitowane zostało zdarzenie. Ma ono posłużyć
do wysłania notyfikacji mailowej.

Zaimplementuj wysyłanie powiadomienia podobnie jak w zadaniu 3.

Dopisz testy jednostkowe dla wydarzenia.

### Zadanie 7

Zaimplementuj kontroler restowy do zarządzania wydarzeniami i rezerwacjami oraz kontroler 
do kategorii.

Dopisz testy integracyjne samych kontrolerów.

### Zadanie 8

Waliduj dane:

* email musi być emailem
* nazwa wydarzenia nie może być pusta

Obsłuż błędy walidacji zwracając `400 Bad request` ze strukturą danych
zawierającą opis błędów. Możesz użyć do tego `@ControllerAdvice`.

Napisz testy integracyjne `@ControllerAdvice` w izolacji.

### Zadanie 9

Pozwól na filtrowanie wydarzeń po nazwie kategorii za pomocą query params.

### Zadanie 10

Zastąp repozytoria InMemory przez `JPA` i SpringData.
